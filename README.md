# kube-helpers

## How to use it

```yaml
stages:
  - 🚢k8s-deploy
  - 🚢undeploy

include:
  - project: 'tanuki-workshops/kube-demos/kube-helpers'
    ref: master
    file: 'k8s.dsl.yml'
# 👋 it's a "before script"

variables:
  APPLICATION_IMAGE: registry.gitlab.com/tanuki-workshops/kube-demos/small-faas:latest
  FUNCTION_CODE: main.js
  FUNCTION_NAME: main
  FUNCTION_LANG: js
  CONTENT_TYPE: application/json;charset=UTF-8
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80
  REPLICAS: 1

🚀:deploy:service:
  stage: 🚢k8s-deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual
  allow_failure: true
  environment:
    name: prod/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
  script:
    - create_configmap_function
    - generate_manifest
    - apply

❌:remove:service:
  stage: 🚢undeploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual
  allow_failure: true
  environment:
    name: prod/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
  script:
    - delete_deployment

```
